# Todo

[x] `CalculateDueDate` method
[x] inputs: input submit date/time and turnaround time
[x] output: return date/time when the issue was resolved
[x] problem can only be reported during working hours
[x] turnaround time is defined in working hours (e.g. 2day is 16 hours)
[x] working hours 9AM to 5PM Mon-Fri
[-] holidays are ignored (holiday on Thursday or working day on Saturday doesn't count)

[x] extend Date to implement addition
[x] switch to ISOString for validation
[x] get rid of `toISOString()`
[x] rename withinBusinessHours to isBusinessHour (or negated)
[x] it is time to introduce class for due date calculation
[x] use add Days instead of addHours(48)
[-] name the new Date class to BusinessDateTime
[-] API user shouldn't know about DateTime - don't know how to do this in nodejs
