const expect = require('chai').expect
const calculateDueDate = require('../src/dueDate').calculateDueDate
const DueDateCalculator = require('../src/dueDate').DueDateCalculator
const DateTime = require('../src/dateUtils').DateTime

describe('Due Date calculator', () => {

    it('should calculate due date', () => {
        const t10_46 = new DateTime("2020-12-07T10:46")
        expect(calculateDueDate(t10_46, 1).toStr()).to.equal('12/7 11:46')
        expect(calculateDueDate(t10_46, 2).toStr()).to.equal('12/7 12:46')
        expect(calculateDueDate(t10_46, 5).toStr()).to.equal('12/7 15:46')
    })

    it('should calculate due date for edge cases', () => {
        const t9_00 = new DateTime("2020-12-07T09:00")
        const t16_59 = new DateTime("2020-12-07T16:59")
        expect(calculateDueDate(t9_00, 8).toStr()).to.equal('12/8 9:00')
        expect(calculateDueDate(t16_59, 8).toStr()).to.equal('12/8 16:59')
    })

    it('should not count weekend days as work days', () => {
        const d12_10_thu = new DateTime("2020-12-10T10:46")
        expect(calculateDueDate(d12_10_thu, 8).toStr()).to.equal('12/11 10:46')
        expect(calculateDueDate(d12_10_thu, 16).toStr()).to.equal('12/14 10:46')
        expect(calculateDueDate(d12_10_thu, 24).toStr()).to.equal('12/15 10:46')
        expect(calculateDueDate(d12_10_thu, 32).toStr()).to.equal('12/16 10:46')
        expect(calculateDueDate(d12_10_thu, 64).toStr()).to.equal('12/22 10:46')
    })

    it('should implement business hour check', () => {
        let ddc = new DueDateCalculator()
        expect(ddc.isOutsideBusinessHours(new Date("2020-12-07T08:59"))).to.be.true
        expect(ddc.isOutsideBusinessHours(new Date("2020-12-07T09:00"))).to.be.false
        expect(ddc.isOutsideBusinessHours(new Date("2020-12-07T16:59"))).to.be.false
        expect(ddc.isOutsideBusinessHours(new Date("2020-12-07T17:00"))).to.be.true
    })

    it('should throw an error if outside of business hours', () => {
        const submitDate = new DateTime("2020-12-07T08:00")
        expect(() => {
            calculateDueDate(submitDate, 1)
        }).to.throw("outside of business hours");
    })
})
