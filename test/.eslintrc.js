module.exports = {
    "env": {
        "mocha": true,
    },
    "rules": {
        "no-unused-vars": [
            "error",
            {
                "varsIgnorePattern": "expect"
            }
        ]
    }
}
