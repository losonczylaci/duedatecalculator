
const expect = require('chai').expect
const DateTime = require('../src/dateUtils').DateTime
const toMinTwoDigits = require('../src/dateUtils').toMinTwoDigits

describe('Date Time class', () => {

    it('should implement hour addition', () => {
        const dt11_59 = new DateTime("2020-12-07T11:59")
        expect(dt11_59.addHours(1).toStr()).to.equal("12/7 12:59")
        expect(dt11_59.addHours(5).toStr()).to.equal("12/7 16:59")
        expect(dt11_59.addHours(24).toStr()).to.equal("12/8 11:59")
        expect(dt11_59.addHours(48).toStr()).to.equal("12/9 11:59")
        expect(dt11_59.addHours(-1).toStr()).to.equal("12/7 10:59")
    })

    it('should implement day addition', () => {
        const dt12_7 = new DateTime("2020-12-07T11:59")
        expect(dt12_7.addDays(1).toStr()).to.equal("12/8 11:59")
    })

    it('should implement weekend check', () => {
        expect(new DateTime("2020-12-11T10:00").isSaturday()).to.be.false
        expect(new DateTime("2020-12-12T10:00").isSaturday()).to.be.true
        expect(new DateTime("2020-12-13T10:00").isSaturday()).to.be.false
        expect(new DateTime("2020-12-14T10:00").isSaturday()).to.be.false
    })

    it('should stringify to minimum to digits', () => {
        expect(toMinTwoDigits(0)).to.equal("00")
        expect(toMinTwoDigits(1)).to.equal("01")
        expect(toMinTwoDigits(10)).to.equal("10")
    })
})
