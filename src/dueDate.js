class DueDateCalculator {

    constructor(startHour = 9, endHour = 17) {
        this.startHour = startHour
        this.endHour = endHour
    }

    getDueDate(submitDateTime, turnaroundTime) {
        if (this.isOutsideBusinessHours(submitDateTime)) {
            throw new Error("Submit date is outside of business hours")
        }
        return this.calcDueDate(submitDateTime, turnaroundTime)
    }

    isOutsideBusinessHours(date) {
        const isEarly = date.getHours() < this.startHour
        const isLate = date.getHours() >= this.endHour
        return isEarly || isLate
    }

    calcDueDate(date, hours) {
        date = date.toMondayIfSaturday()
        const remainingHours = this.getRemainingHours(date, hours)

        if (remainingHours < 0) {
            return date.addHours(hours)
        }

        const tomorrowMorning = this.getTomorrowMorningDate(date)
        return this.calcDueDate(tomorrowMorning, remainingHours)
    }

    getTomorrowMorningDate(date) {
        const hoursSinceStartHour = date.getHours() - this.startHour
        return date.addDays(1).addHours(-hoursSinceStartHour)
    }

    getRemainingHours(date, hours) {
        const hoursTillEod = this.endHour - date.getHours()
        const remaining = hours - hoursTillEod
        return remaining
    }
}

function calculateDueDate(submitDateTime, turnaroundTime) {
    return new DueDateCalculator().getDueDate(submitDateTime, turnaroundTime)
}

module.exports = { calculateDueDate, DueDateCalculator }
