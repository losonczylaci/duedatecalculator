
class DateTime extends Date {
    addHours(hours) {
        let date = new DateTime(this.getTime());
        date.setHours(date.getHours() + hours)
        return date
    }

    addDays(days) {
        return this.addHours(24 * days)
    }

    toStr() {
        let m = this.getMonth() + 1
        let d = this.getDate()
        let h = this.getHours()
        let min = toMinTwoDigits(this.getMinutes())
        return `${m}/${d} ${h}:${min}`
    }

    toMondayIfSaturday() {
        if (this.isSaturday())
            return this.addDays(2)
        return this
    }

    isSaturday() {
        return this.getDay() === 6
    }
}

function toMinTwoDigits(number) {
    if (number < 10)
        return `0${number}`
    return String(number)
}

module.exports = { DateTime, toMinTwoDigits }
